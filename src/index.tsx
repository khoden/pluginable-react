import React from 'react';
import ReactDOM from 'react-dom';

import { Application, ApplicationProvider } from './app';
import './design/index.css';
import App from './layout/App/App';
import { HomePagePlugin } from './plugins/homePage';
import { Page1Plugin } from './plugins/page1Page';
import { PluginListPlugin } from './plugins/pluginListPage';
import * as serviceWorker from './serviceWorker';

const app = new Application('Plugin test');

app.registerPlugin(new HomePagePlugin());
app.registerPlugin(new PluginListPlugin());
app.registerPlugin(new Page1Plugin());

ReactDOM.render(
   <React.StrictMode>
      <ApplicationProvider app={app}>
         <App />
      </ApplicationProvider>
   </React.StrictMode>,
   document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
