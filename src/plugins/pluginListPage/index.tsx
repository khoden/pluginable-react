import React from 'react';
import { Link } from 'react-router-dom';
import { Application, BasePlugin } from '../../app';
import { PluginListPage } from './PluginListPage';

export class PluginListPlugin extends BasePlugin {
   constructor() {
      super({
         name: 'pluginListPlugin',
         version: '0.0.1',
      });
   }

   public register(app: Application): void {
      super.register(app);

      const header = app.getHeader();

      this.subscriptions.push(
         app.registerPage({ component: PluginListPage, path: '/plugins' }),
         header.addItem('plugins', 1000, <Link to={'/plugins'}>Plugins</Link>),
      );
   }
}
