import React, { useCallback } from 'react';
import { useApp } from '../../app';
import { PluginItem } from './PluginItem';

export function PluginListPage() {
   const app = useApp();

   const handleRemovePlugin = useCallback((name: string, version: string) => {
      console.warn('TODO unregister plugin', name, version);
      // app.unregisterPlugin(); // TODO
   }, []);

   return (
      <table>
         <thead>
            <tr>
               <th>Name</th>
               <th>Version</th>
               <th>Actions</th>
            </tr>
         </thead>
         <tbody>
            {app.getPluginList().map(plugin => (
               <PluginItem
                  key={plugin.name}
                  plugin={plugin}
                  onRemove={handleRemovePlugin}
               />
            ))}
         </tbody>
      </table>
   );
}
