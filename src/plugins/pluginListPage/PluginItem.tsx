import React, { FC } from 'react';
import { PluginRecord } from '../../app';

interface Props {
   plugin: PluginRecord;

   onRemove(name: string, version: string): void;
}

export const PluginItem: FC<Props> = ({ plugin, onRemove }) => {
   const handleRemove = () => onRemove(plugin.name, plugin.version);

   return (
      <tr key={plugin.name}>
         <td>{plugin.name}</td>
         <td>{plugin.version}</td>
         <td>
            <button onClick={handleRemove}>remove</button>
         </td>
      </tr>
   );
};
