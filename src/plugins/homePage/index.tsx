import { Application, BasePlugin } from '../../app';
import { HomePage } from './HomePage';

export class HomePagePlugin extends BasePlugin {
   constructor() {
      super({
         name: 'homePagePlugin',
         version: '0.0.1',
      });
   }

   public register(app: Application): void {
      super.register(app);

      this.subscriptions.push(
         app.registerPage({
            component: HomePage,
            path: '/',
            exact: true,
            weight: 1000,
         }),
      );
   }
}
