import React from 'react';
import { Link } from 'react-router-dom';
import { Application, BasePlugin } from '../../app';
import { Page1 } from './Page1';

export class Page1Plugin extends BasePlugin {
   constructor() {
      super({
         name: 'page1Plugin',
         version: '0.0.1',
      });
   }

   public register(app: Application): void {
      super.register(app);

      const header = app.getHeader();

      this.subscriptions.push(
         app.registerPage({ component: Page1, path: '/page1' }),
         header.addItem('page1', 0, <Link to={'/page1'}>Page1</Link>),
         header.addItem('unknown', 1e6, <Link to={'/404'}>404</Link>),
      );
   }
}
