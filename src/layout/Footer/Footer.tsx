import classNames from 'classnames';
import React, { FC } from 'react';
import classes from './Footer.module.css';


interface Props {
    className?: string;
}


export const Footer: FC<Props> = ({className}) => {
    return (
        <footer className={classNames(classes.footer, className)}>
            TODO footer
        </footer>
    );
};
