import classNames from 'classnames';
import React, { FC, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { useApp } from '../../app';
import classes from './Header.module.css';

interface Props {
   className?: string;
}

export const Header: FC<Props> = ({ className }) => {
   const app = useApp();
   const header = useMemo(() => app.getHeader(), [app]);
   const logo = useMemo(() => header.getLogo(), [header]);

   return (
      <header className={classNames(classes.header, className)}>
         <div className={classes.logo}>
            {typeof logo === 'string' ? (
               <Link to={'/'} className={classes.textLogo}>
                  {logo}
               </Link>
            ) : (
               { logo }
            )}
         </div>

         {header.items.map(item => (
            <React.Fragment key={item.key}>{item.node}</React.Fragment>
         ))}
      </header>
   );
};
