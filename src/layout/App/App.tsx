import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { ApplicationRoutes } from '../../app';
import { Footer } from '../Footer/Footer';
import { Header } from '../Header/Header';
import classes from './App.module.css';

function App() {
   return (
      <BrowserRouter>
         <Header />

         <main className={classes.main}>
            <ApplicationRoutes />
         </main>

         <Footer />
      </BrowserRouter>
   );
}

export default App;
