import { ReactNode } from 'react';

import { HeaderItemAlreadyRegistered } from '../errors';
import { PublicApplicationApi } from '../PublicApplicationApi';
import { RegisterUnsubscription } from '../RegisterUnsubscription';
import { BasePart } from './BasePart';

interface HeaderItem {
   node: ReactNode;
   weight: number;
   key: string;
}

export class HeaderPart extends BasePart {
   private itemKeys = new Set<string>();
   public items = [] as HeaderItem[];
   private logo: ReactNode;

   constructor(private app: PublicApplicationApi) {
      super();
   }

   public getLogo(): ReactNode {
      return this.logo ?? this.app.getName();
   }

   public useLogo(logo: ReactNode) {
      this.logo = logo;
   }

   public addItem(
      key: string,
      weight: number,
      node: ReactNode,
   ): RegisterUnsubscription {
      if (this.itemKeys.has(key)) {
         throw new HeaderItemAlreadyRegistered(key);
      }

      const item: HeaderItem = { key, weight, node };
      this.app.update(() => {
         this.items.push(item);
         this.items.sort((a, b) => a.weight - b.weight);
      });

      return () => {
         this.app.update(() => {
            const i = this.items.indexOf(item);
            this.items.slice(i, 1);
         });
      };
   }
}
