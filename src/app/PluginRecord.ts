import { Version } from './Version';

export interface PluginRecord {
   name: string;
   version: Version;
}
