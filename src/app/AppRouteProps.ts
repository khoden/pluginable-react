import { RouteProps } from 'react-router-dom';

export interface AppRouteProps
   extends Pick<RouteProps, 'path' | 'exact' | 'component'> {
   weight?: number;
}
