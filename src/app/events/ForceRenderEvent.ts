import { Application } from '../Application';
import { ApplicationEvent } from './ApplicationEvent';

export class ForceRenderEvent implements ApplicationEvent<void, Application> {
   public static type = 'ForceRender';

   public type = ForceRenderEvent.type;
   public payload: void;
   public target: Application;

   constructor(app: Application) {
      this.target = app;
   }
}
