export interface ApplicationEvent<Payload = any, Target = any> {
   type: string;
   target: Target;
   payload: Payload;
}
