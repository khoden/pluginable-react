import { BasePlugin } from '../BasePlugin';

export class PluginAlreadyRegistered extends Error {
   constructor(plugin: BasePlugin) {
      super(
         `Plugin "${plugin.name}" version ${plugin.version} already registered.`,
      );
   }
}
