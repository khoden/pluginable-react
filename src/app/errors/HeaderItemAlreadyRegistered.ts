export class HeaderItemAlreadyRegistered extends Error {
   constructor(key: string) {
      super(`HeaderItem "${key}" already registered.`);
   }
}
