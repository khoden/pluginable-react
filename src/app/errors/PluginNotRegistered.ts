import { BasePlugin } from '../BasePlugin';

export class PluginNotRegistered extends Error {
   constructor(plugin: BasePlugin) {
      super(
         `Plugin "${plugin.name}" version ${plugin.version} already registered.`,
      );
   }
}
