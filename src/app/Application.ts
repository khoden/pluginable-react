import { Subject } from 'rxjs';

import { AppRouteProps } from './AppRouteProps';
import { BasePlugin } from './BasePlugin';
import { PluginAlreadyRegistered, PluginNotRegistered } from './errors';
import { ApplicationEvent } from './events/ApplicationEvent';
import { ForceRenderEvent } from './events/ForceRenderEvent';
import { HeaderPart } from './parts/Header';
import { PluginRecord } from './PluginRecord';
import { PublicApplicationApi } from './PublicApplicationApi';
import { RegisterUnsubscription } from './RegisterUnsubscription';

export class Application implements PublicApplicationApi {
   public events = new Subject<ApplicationEvent>();
   public routes: AppRouteProps[] = [];
   private readonly header: HeaderPart;
   private plugins = new Map<string, BasePlugin>();

   constructor(private name: string) {
      this.header = new HeaderPart(this);
   }

   public registerPlugin(plugin: BasePlugin): RegisterUnsubscription {
      const key = BasePlugin.getKey(plugin);
      if (this.plugins.has(key)) {
         throw new PluginAlreadyRegistered(plugin);
      }

      this.update(() => {
         this.plugins.set(key, plugin);

         plugin.register(this);
      });

      return () => this.unregisterPlugin(key, plugin);
   }

   public registerPage(routeProps: AppRouteProps): RegisterUnsubscription {
      this.update(() => {
         this.routes.push(routeProps);

         this.routes.sort((a, b) => (a.weight ?? 0) - (b.weight ?? 0));
      });

      return () => this.unregisterPage(routeProps);
   }

   public getPluginList(): PluginRecord[] {
      return Array.from(this.plugins.values()).map(p => ({
         name: p.name,
         version: p.version,
      }));
   }

   public getName(): string {
      return this.name;
   }

   public setName(name: string): void {
      this.update(() => (this.name = name));
   }

   public getHeader(): HeaderPart {
      return this.header;
   }

   private forceRender(): void {
      this.events.next(new ForceRenderEvent(this));
   }

   private unregisterPage(routeProps: AppRouteProps) {
      this.update(() => {
         const i = this.routes.indexOf(routeProps);
         this.routes.splice(i, 1);
      });
   }

   private unregisterPlugin(key: string, plugin: BasePlugin): void {
      const registeredPlugin = this.plugins.get(key);
      if (!registeredPlugin) {
         throw new PluginNotRegistered(plugin);
      }

      this.update(() => {
         this.plugins.delete(key);

         registeredPlugin.unregister();
      });
   }

   public update(cb: () => void) {
      cb();

      this.forceRender();
   }
}
