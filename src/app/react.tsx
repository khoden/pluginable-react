import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import { filter } from 'rxjs/operators';

import { Application } from './Application';
import { ApplicationNotFoundInContext } from './errors';
import { ForceRenderEvent } from './events/ForceRenderEvent';
import { PublicApplicationApi } from './PublicApplicationApi';

const ApplicationContext = React.createContext<Application | null>(null);

interface Props {
   app: Application;
}

export const ApplicationProvider: FC<Props> = ({ app, children }) => {
   return (
      <ApplicationContext.Provider value={app}>
         {children}
      </ApplicationContext.Provider>
   );
};

function useForceRender() {
   const [, setCounter] = useState(0);

   return useCallback(() => setCounter(c => c + 1), []);
}

export function useApp(): PublicApplicationApi {
   const app = useContext(ApplicationContext);
   const forceRender = useForceRender();
   if (!app) {
      throw new ApplicationNotFoundInContext();
   }

   useEffect(() => {
      const s = app.events
         .pipe(filter(e => e.type === ForceRenderEvent.type))
         .subscribe(forceRender);

      return () => s.unsubscribe();
   });

   return app;
}

function Page404() {
   return (
      <>
         <h1>Error 404: Page not found</h1>
      </>
   );
}

export const ApplicationRoutes: FC = () => {
   const app = useApp();

   return (
      <Switch>
         {app.routes.map(routeProps => (
            <Route key={routeProps.path as string} {...routeProps} />
         ))}
         <Route component={Page404} />
      </Switch>
   );
};
