import { AppRouteProps } from './AppRouteProps';
import { BasePlugin } from './BasePlugin';
import { HeaderPart } from './parts/Header';
import { PluginRecord } from './PluginRecord';

export interface PublicApplicationApi {
   routes: AppRouteProps[];

   getHeader(): HeaderPart;

   getName(): string;

   getPluginList(): PluginRecord[];

   registerPage(routeProps: AppRouteProps): () => void;

   registerPlugin(plugin: BasePlugin): () => void;

   setName(name: string): void;

   update(cb: () => void): void;
}
