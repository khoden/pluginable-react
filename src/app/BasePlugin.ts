import { Application } from './Application';
import { PluginRecord } from './PluginRecord';
import { PublicApplicationApi } from './PublicApplicationApi';
import { RegisterUnsubscription } from './RegisterUnsubscription';
import { Version } from './Version';

export abstract class BasePlugin {
   public name: string;
   public version: Version;
   protected app: PublicApplicationApi | undefined;

   protected subscriptions: RegisterUnsubscription[] = [];

   protected constructor(record: PluginRecord) {
      this.name = record.name;
      this.version = record.version;
   }

   public static getKey(record: PluginRecord) {
      return `${record.name}_v${record.version}`;
   }

   public register(app: Application) {
      this.app = app;
   }

   public unregister(): void {
      this.subscriptions.forEach(s => s());
   }
}
